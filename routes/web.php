<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('client', [\App\Http\Controllers\SubscriptionController::class, 'index1' ]);
Route::get('client/{client}', [\App\Http\Controllers\SubscriptionController::class, 'client' ]);

Route::get('/contacts', [\App\Http\Controllers\SubscriptionController::class, 'contacts' ]);
Route::get('/about_us', [\App\Http\Controllers\SubscriptionController::class, 'about' ]);

Route::get('adding', [\App\Http\Controllers\SubscriptionController::class, 'adding' ]);
Route::get('/del', [\App\Http\Controllers\SubscriptionController::class, 'destroy' ]);
Route::get('/edit', [\App\Http\Controllers\SubscriptionController::class, 'editing' ]);
Route::get('/editconfirming', [\App\Http\Controllers\SubscriptionController::class, 'editingconfirm' ]);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('/admin', function ()
{
    if(Auth::check())
        return view('Admin.layout',[\App\Http\Controllers\SubscriptionController::class, 'index1' ]);
    else
        return redirect('/home');
});

Route::get("/admin/client",[\App\Http\Controllers\SubscriptionController::class, 'index1' ]);

