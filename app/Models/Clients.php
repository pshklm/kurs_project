<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Clients extends Model
{
    use HasFactory;

    public static $abonement_types = array(
        '1' => 'VIP',
        '2' => 'Pool',
        '3' => 'Gym',
        '4' => 'Beginer'
    );

    public static $regular_types = array(
        '1' => 'non permanent',
        '2' => 'permanent'
    );


    public function deleteClient($name)
    {

        $query = DB::select('DELETE FROM client WHERE client_id = $client_id');
        $deleted = $query->get();
        return $deleted;
    }

//    public function addClient($fio, $telephone, $subs)
//    {
//
////        DB::insert('insert into client (name, tel, type_sbs, date_s, date_e, regular) values (?,?,?,?,?,?)', [$fio, $telephone, $subs, null, null,1]);
//        $today = date("j, n, Y");
//        $values = array('name' => $fio, 'tel' => $telephone, 'type_sbs' => $subs, 'date_s' => $today, 'date_e' => $today, 'regular' => '1');
//
//        DB::table('client')
//            ->insert($values);
//
//        return $values;
//
//    }


    public function getClients($type, $regular)
    {
        $query = DB::table('fitnescenter.client');

        $query->select('subscription.*', 'client.name', 'client.client_id', 'client.tel', 'client.date')
            ->join('subscription', 'client.type_sbs', '=', 'subscription.subscription_id');

        if ($type) {
            $query->where('type_sbs', '=', $type);
        }

        if ($regular) {
            $query->where('regular', '=', $regular);
        }

        $clients = $query->get();

        return $clients;
    }

    public function getClientByID($id)
    {
        if (!$id) return null;
        $client = DB::table('client')
            ->select('*')
            ->join('subscription', 'client.type_sbs', '=', 'subscription.subscription_id')
            ->where('client_id', $id)
            ->get()->first();
        return $client;
    }

//    public function getAbonements(){
//        return DB::select('select distinct (type) from subscription order by type');
//    }
//
//    public function getRegularTypesForClients()
//    {
//        return DB::select('select distinct(regular) from client order by regular');
//    }
}
